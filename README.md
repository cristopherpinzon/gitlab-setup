### Command line instructions

##### Git global setup
``` bash
git config --global user.name "YOUR NAME"
git config --global user.email "your.email@gmail.com"
```
##### Create a new repository
``` bash
git clone git@gitlab.com:your.account/test.git
cd test
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```
##### Existing folder
``` bash
cd existing\_folder
git init
git remote add origin git@gitlab.com:your.account/test.git
git add .
git commit -m "Initial commit"
git push -u origin master
```
##### Existing Git repository
``` bash
cd existing\_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:your.account/test.git
git push -u origin --all
git push -u origin --tags
```
